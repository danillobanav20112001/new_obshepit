#include "registration.h"
#include <QSettings>
#include <QtSql>
#include <qString>
#include <QtDebug>
#include <QFileInfo>
#include "QSqlDatabase"
#include "QSqlQuery"
#include <QSqlQueryModel>
#include <QMessageBox>
#include "starting.h"

//�������� ��������� �� �������� ����������� � ������� ������ �� �� � ���� ������ � ������
void registration::settings_window() {
    QIcon winIcon("ICON.bmp");
    this->setWindowIcon(winIcon);
    QPixmap bkgnd("background3.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
    ui->registr->setStyleSheet("QPushButton{background: white; border:none;}"
        "QPushButton:hover{background: #505050; color:white;}");
    
}

registration::registration(QWidget *parent)
	: ui(new Ui::registration)
{
	ui->setupUi(this);
    settings_window();
    
    //starting* start=new starting();
    //connect(this, &registration::send_data, start, &starting::send_data);
    //emit send_data("12","2");
    QSqlDatabase registr = QSqlDatabase::addDatabase("QSQLITE");
    registr.setDatabaseName("dbcompany.sqlite");

    if (!registr.open())
    {
        qDebug() << registr.lastError().text();
        ui->status->setText(QString::fromUtf8(u8"����������� �� �������"));
        return;
    }
    else {
        ui->status->setText(QString::fromUtf8(u8"�����������")); //registr.close();
    }
    QSqlQuery query=QSqlQuery(registr);
    query.prepare(u8"CREATE TABLE �������� (id INTEGER UNIQUE PRIMARY KEY, �������� VARCHAR(30), Email VARCHAR(30), ����� VARCHAR(30),������ VARCHAR(30), ������������ VARCHAR(30), �������������� VARCHAR(4), �������� VARCHAR(30))");
    if (query.exec())
    {
       // ui->status->setText(u8"�����������");
    }
    else
    {
        qDebug() << registr.lastError().text();
       // ui->status->setText(u8"����������� �� �������");
    }
    
    this->connect(ui->registr, &QPushButton::clicked, [ui = ui, thisP = this]() {

        QString str = ui->electronn->text().toLower();
        QString tmpl = "@";
        QRegExp* q = new QRegExp(tmpl);
        if (q->indexIn(str) == -1) {//���� email ��� ����� @ - �� ������ ��������� � �� ���� ������ ��������
            QMessageBox::information(thisP, QObject::tr("Error!"), u8"����������, ������� ���������� e-mail!");
        }
        else if (!thisP->check_login()) {
            qDebug() << "��� ��������, ������� ������� ����� =(";
        }
        else if (ui->name_comp->text() == "") {
            QMessageBox::information(thisP, QObject::tr("Error!"), u8"����������, ������� �������� ��������");
        }
        else if (ui->login->text()=="") {
            QMessageBox::information(thisP, QObject::tr("Error!"), u8"����������, ������� �����");
        }
        else if (ui->password->text()=="") {
            QMessageBox::information(thisP, QObject::tr("Error!"), u8"����������, ������� ������");
        }
        else {
            auto text_check = thisP->how_checked_radiobutton();//�������� �� ������� ������ ������ ���� ������������
            if (text_check == "no_check") {
                QMessageBox::information(thisP, QObject::tr("Error!"), u8"�� ������ ������� ��� ������������.");
            }
            else {
                QSqlDatabase registr = QSqlDatabase::addDatabase("QSQLITE");
                registr.setDatabaseName("dbcompany.sqlite");
                registr.open();
                QSqlQuery query = QSqlQuery(registr);

                query.prepare(u8"INSERT INTO  �������� (��������, Email, �����, ������, ������������, ��������������, ��������) VALUES(?, ?, ?, ?, ?, ?, ?)"); // �������������� ������
                query.addBindValue(ui->name_comp->text());
                query.addBindValue(ui->electronn->text().toLower());
                query.addBindValue(ui->login->text());
                query.addBindValue(ui->password->text());
                query.addBindValue(text_check);
                query.addBindValue(u8"1111");//�� ��������� 1111 ����� ������������ ��� ������ ��� �������� ������� ��������������
                query.addBindValue("01.04.20400");//���� ��������� ��������, �� ��������� � �� ������ ����, �� ���� �������� �������
                //����� ���������� ����� �������� � ��� ....... ������� ������

                if (query.exec())
                {
                    QSettings settings("settings.ini", QSettings::IniFormat);
                    settings.beginGroup("Autorisation"); //��������� � ��������� ������ ��� ���������� �����������
                    settings.setValue("login", ui->login->text());
                    settings.setValue("password", ui->password->text());
                    settings.setValue("checkBox_autorisa", false);
                    settings.endGroup();
                    thisP->close();
                    QFile file("settings2.py");
                    if (file.open(QIODevice::WriteOnly)) /*��������� ���� � ������ ������ ��� ������ � �����. � ���� �*/
                    {
                        QByteArray data;
                        data = QString("\nto_email = ").toUtf8();
                        data.append(QString("'%1'").arg(ui->electronn->text().toLower()));
                        data.append(QString("\n"));
                        data.append(QString("subject = "));
                        data.append(QString(u8"'������������� �����������'"));
                        data.append(QString("\n"));
                        data.append(QString("message = "));
                        data.append(QString(u8"'����������� ����������� � ��������� ���, �������, ��� �� ������ � ����. ��� �����: %1. ��� ������: %2.'")
                            .arg(ui->login->text(), ui->password->text()));//��������� ���������� ����� �����������
                        //���� ����� �������� ��� ���������********************************************************
                        file.write(data); /*���������� ������*/
                        file.close(); /*��������� ����*/
                    }
                    registr.close();
                   thisP->connect_db_company_name(); //������ ��� ���� ������
                   /*
                   ////////////////////////////////////////
                   ������ � �� ���� ��������� � ������ ����� � �������� ���� ��������
                   ////////////////////////////////////////
                   */


                    QMessageBox::information(thisP, QObject::tr(u8"����������� �������"),
                        u8"������� �� �����������! \n ��������� ���� e-mail ��� ������������� �����������");


                    // QProcess* proc = new QProcess(thisP); //�������� �������� ��������� �� �����
                    QProcess* myProcess = new QProcess(thisP);
                    auto cmd = ("python contact_me.pyw");
                    myProcess->start(cmd);
                    // auto cmd=("python contact_me.pyw"); //��� ���� �� �������� ���������
                    // system(cmd);
                    starting* start = new starting();
                    start->show();
                    //  emit thisP->send_data(ui->login->text(), ui->password->text());
                }
                else
                {
                    qDebug() << registr.lastError().text();
                    QMessageBox::information(thisP, QObject::tr("Error!"), u8"�����, �� ���������� ������ �����������, \n �������� ��� ����� � ��������");
                }
            }
        }
        
    });
}

QString registration::how_checked_radiobutton() { //��������� �� �����-�������, ������ ������� � ��������� � ��������
    auto no_check = 0;
    QString check_obshepit = "";
    QList<QRadioButton*> list = ui->groupBox->findChildren<QRadioButton*>();
    for (int i = 0; i < 8; i++) {
        QRadioButton* rb = list.at(i);
        if (rb->isChecked()) {
            check_obshepit = rb->text();
            no_check++;
        }
    }
    if (no_check > 0) {
        return check_obshepit;
    }
    else {
        return "no_check";
    }
    
}

bool registration::check_login() {//�������� ����� �� ���������� �� ��
    QSqlDatabase autorisation = QSqlDatabase::addDatabase("QSQLITE");
    autorisation.setDatabaseName("dbcompany.sqlite");
    autorisation.open();
    QSqlQuery query = QSqlQuery(autorisation);
    if (query.exec(u8"SELECT * FROM ��������")) {
        while (query.next()) { 
            if (query.value(3) == ui->login->text()) {
                
                bad_message_login();
                return 0;
            }
            else if (query.value(2) == ui->electronn->text().toLower()) {
                bad_message_email();
                return 0;
            }
        }
        return 1;
    }
    else {
        return 1;
    }

}

void registration::bad_message_login() {
    
    QMessageBox::information(this, QObject::tr(u8"������ ������� �����!"), u8"������ ����� ��� ���������������, �������� ���������� ������.");
}

void registration::bad_message_email() {

    QMessageBox::information(this, QObject::tr(u8"������ ������� e-mail!"), u8"������ e-mail ��� ���������������, �������� ���������� ������.");
}

registration::~registration()
{
    delete ui;

}

void registration::connect_db_company_name() {
    QSqlDatabase autoriz_personal = QSqlDatabase::addDatabase("QSQLITE");
    autoriz_personal.setDatabaseName("dbpersonal.sqlite");

    if (!autoriz_personal.open())
    {
        //qDebug() << autoriz_personal.lastError().text();
        qDebug() << u8"�������� � ��������� ���� ������";
        //ui->status->setText(QString::fromUtf8(u8"����������� �� �������"));
        return;
    }
    else {
        qDebug() << u8"�� �������";
        //ui->status->setText(QString::fromUtf8(u8"�����������")); //registr.close();
    }
    QSqlQuery query = QSqlQuery(autoriz_personal);
    QString autoriz_company_personal = ui->name_comp->text();
    autoriz_company_personal.replace(QRegExp(" "), "_");//������� ������ �� ������ �������������
    query.prepare(u8"CREATE TABLE "+ autoriz_company_personal + u8" (id INTEGER UNIQUE PRIMARY KEY, ��� VARCHAR(30), ������ VARCHAR(30), ��������� VARCHAR(30))");
    ////////////////////////////////////////////////
    //�������� ���                                //
    ////////////////////////////////////////////////

    if (query.exec())
    {
        //autoriz_personal.close();
        query.prepare(u8"INSERT INTO "+ autoriz_company_personal +u8" (���, ������, ���������) VALUES(?, ?, ?)"); // �������������� ������
        query.addBindValue(u8"�����");
        query.addBindValue(u8"1111");
        query.addBindValue(u8"��������");
        if (query.exec())
        {
            qDebug() << "good";
        }
        else {
            qDebug() << autoriz_personal.lastError().text();
        }
        
    }
    else
    {
         qDebug() << autoriz_personal.lastError().text();
       
    }
   
}