#pragma once

#include <QWidget>
#include "ui_registration.h"

class registration : public QWidget
{
	Q_OBJECT

public:
	registration(QWidget *parent = Q_NULLPTR);
	~registration();

private:

	Ui::registration *ui;
	QString how_checked_radiobutton();
	void settings_window();
	bool check_login();
	void bad_message_login();
	void bad_message_email();
	void connect_db_company_name();
//signals:
	//void send_data(QString login, QString password);
};
