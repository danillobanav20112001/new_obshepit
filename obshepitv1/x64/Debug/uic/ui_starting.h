/********************************************************************************
** Form generated from reading UI file 'starting.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STARTING_H
#define UI_STARTING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_startingClass
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QGroupBox *groupBox;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QPushButton *pushButton;
    QLabel *label_password;
    QLineEdit *your_password;
    QLineEdit *your_login;
    QLabel *label_login;
    QCheckBox *checkBox_auto_input;
    QGroupBox *groupBox_2;
    QPushButton *pushButton_3;
    QLabel *label_6;

    void setupUi(QMainWindow *startingClass)
    {
        if (startingClass->objectName().isEmpty())
            startingClass->setObjectName(QString::fromUtf8("startingClass"));
        startingClass->setWindowModality(Qt::NonModal);
        startingClass->setEnabled(true);
        startingClass->resize(414, 300);
        startingClass->setMaximumSize(QSize(417, 16777215));
        QFont font;
        font.setFamily(QString::fromUtf8("Nirmala UI"));
        startingClass->setFont(font);
        startingClass->setCursor(QCursor(Qt::ArrowCursor));
        startingClass->setMouseTracking(false);
        startingClass->setTabletTracking(false);
        startingClass->setFocusPolicy(Qt::TabFocus);
        startingClass->setContextMenuPolicy(Qt::NoContextMenu);
        startingClass->setAcceptDrops(false);
        startingClass->setLayoutDirection(Qt::LeftToRight);
        startingClass->setAutoFillBackground(false);
        startingClass->setStyleSheet(QString::fromUtf8(""));
        startingClass->setToolButtonStyle(Qt::ToolButtonIconOnly);
        startingClass->setTabShape(QTabWidget::Rounded);
        startingClass->setDockNestingEnabled(false);
        startingClass->setUnifiedTitleAndToolBarOnMac(false);
        centralWidget = new QWidget(startingClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 391, 91));
        label->setStyleSheet(QString::fromUtf8("font: 8pt \"Arial\";"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(50, 90, 321, 131));
        groupBox->setStyleSheet(QString::fromUtf8("font: 8pt \"Arial\";"));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 20, 293, 101));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setStyleSheet(QString::fromUtf8("\n"
"font: 8pt \"Arial\";"));

        gridLayout->addWidget(pushButton, 2, 1, 1, 1);

        label_password = new QLabel(layoutWidget);
        label_password->setObjectName(QString::fromUtf8("label_password"));
        label_password->setStyleSheet(QString::fromUtf8("font: 8pt \"Arial\";"));

        gridLayout->addWidget(label_password, 1, 0, 1, 1);

        your_password = new QLineEdit(layoutWidget);
        your_password->setObjectName(QString::fromUtf8("your_password"));
        your_password->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(your_password, 1, 1, 1, 1);

        your_login = new QLineEdit(layoutWidget);
        your_login->setObjectName(QString::fromUtf8("your_login"));

        gridLayout->addWidget(your_login, 0, 1, 1, 1);

        label_login = new QLabel(layoutWidget);
        label_login->setObjectName(QString::fromUtf8("label_login"));
        label_login->setStyleSheet(QString::fromUtf8("font: 8pt \"Arial\";"));

        gridLayout->addWidget(label_login, 0, 0, 1, 1);

        checkBox_auto_input = new QCheckBox(layoutWidget);
        checkBox_auto_input->setObjectName(QString::fromUtf8("checkBox_auto_input"));
        checkBox_auto_input->setStyleSheet(QString::fromUtf8("font: 8pt \"Arial\";"));

        gridLayout->addWidget(checkBox_auto_input, 3, 1, 1, 1);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(50, 230, 321, 41));
        pushButton_3 = new QPushButton(groupBox_2);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(100, 10, 193, 23));
        pushButton_3->setStyleSheet(QString::fromUtf8("font: 8pt \"Arial\";"));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 270, 261, 20));
        label_6->setStyleSheet(QString::fromUtf8("font: 8pt \"Arial\";"));
        startingClass->setCentralWidget(centralWidget);

        retranslateUi(startingClass);

        QMetaObject::connectSlotsByName(startingClass);
    } // setupUi

    void retranslateUi(QMainWindow *startingClass)
    {
        startingClass->setWindowTitle(QCoreApplication::translate("startingClass", "\320\220\320\262\321\202\320\276\321\200\320\270\320\267\320\260\321\206\320\270\321\217", nullptr));
        label->setText(QCoreApplication::translate("startingClass", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">\320\224\320\276\320\261\321\200\320\276 \320\277\320\276\320\266\320\260\320\273\320\276\320\262\320\260\321\202\321\214 \320\262 \320\277\321\200\320\276\320\263\321\200\320\260\320\274\320\274\321\203 \320\264\320\273\321\217</span></p><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">\321\203\320\277\321\200\320\260\320\262\320\273\320\265\320\275\320\270\321\217 \320\270 \320\262\320\265\320\264\320\265\320\275\320\270\321\217 \320\262\320\260\321\210\320\270\320\274 \320\267\320\260\320\262\320\265\320\264\320\265\320\275\320\270\320\265\320\274</span></p></body></html>", nullptr));
#if QT_CONFIG(tooltip)
        groupBox->setToolTip(QCoreApplication::translate("startingClass", "<html><head/><body><p>\320\220\320\262\321\202\320\276\321\200\320\270\320\267\320\260\321\206\320\270\321\217 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\217</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        groupBox->setTitle(QCoreApplication::translate("startingClass", "\320\220\320\262\321\202\320\276\321\200\320\270\320\267\320\260\321\206\320\270\321\217", nullptr));
        pushButton->setText(QCoreApplication::translate("startingClass", "\320\222\320\276\320\271\321\202\320\270", nullptr));
        label_password->setText(QCoreApplication::translate("startingClass", "<html><head/><body><p><span style=\" font-weight:600;\">\320\222\320\260\321\210 \320\277\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", nullptr));
        label_login->setText(QCoreApplication::translate("startingClass", "<html><head/><body><p><span style=\" font-weight:600;\">\320\222\320\260\321\210 \320\273\320\276\320\263\320\270\320\275</span></p></body></html>", nullptr));
        checkBox_auto_input->setText(QCoreApplication::translate("startingClass", "\320\220\320\262\321\202\320\276\320\274\320\260\321\202\320\270\321\207\320\265\321\201\320\272\320\270 \320\267\320\260\321\205\320\276\320\264\320\270\321\202\321\214 \320\277\321\200\320\270 \320\267\320\260\320\277\321\203\321\201\320\272\320\265", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("startingClass", "\320\240\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\321\217", nullptr));
        pushButton_3->setText(QCoreApplication::translate("startingClass", "\320\237\320\265\321\200\320\265\320\271\321\202\320\270 \320\272 \321\200\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\320\270 \320\272\320\276\320\274\320\277\320\260\320\275\320\270\320\270", nullptr));
        label_6->setText(QCoreApplication::translate("startingClass", "Status...[ ]", nullptr));
    } // retranslateUi

};

namespace Ui {
    class startingClass: public Ui_startingClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STARTING_H
