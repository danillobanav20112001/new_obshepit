#pragma once



#include "ui_starting.h"
#include "registration.h"
#include "recover.h"
//#include <MainWindowAPP.h>
//#include <qsqlquery>

namespace Ui {
    class starting;
}
class starting:public QMainWindow
{
    Q_OBJECT
        
public:
    explicit starting(QWidget* parent = nullptr);
    registration* Registr_company;
    recover* Recover;
    //MainWindowAPP* mainWindow;
private:
    
    Ui::startingClass* ui;
    void load_settings();
    void bad_message();
    void good_message();
    void run_autorisation();
    void write_in_file();
    void settings_window();
    void recover_run();
    //static QString getCloseStyleSheet();
public slots:
    void run_reg();
    //void send_data(QString login, QString password);
   // struct startingPD;
   // QScopedPointer<startingPD> pd;


};
