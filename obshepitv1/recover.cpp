#include "recover.h"
#include "QSqlDatabase"
#include "QSqlQuery"
#include "QMessageBox"
#include "QFile"
#include "QProcess"
void recover::settings_window() { //��������� ������� �� ����
    QIcon winIcon("ICON.bmp");
    this->setWindowIcon(winIcon);
    QPixmap bkgnd("background3.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
    ui->recover_2->setStyleSheet("QPushButton{background: white; border:none;}"
        "QPushButton:hover{background: #505050; color:white;}");
}
recover::recover(QWidget *parent)
	: ui(new Ui::recover)
{
	ui->setupUi(this);
    settings_window();
    this->connect(ui->recover_2, &QPushButton::clicked, [ui = ui, thisP = this]() {
        if (thisP->check_login()) {
            QMessageBox::information(thisP, QObject::tr(u8"�������� ������ �������"),
                u8"��������� ���� e-mail!");
            thisP->close();
            QProcess* myProcess = new QProcess(thisP);
            auto cmd = ("python contact_me.pyw");
            myProcess->start(cmd);
            //auto cmd = ("python contact_me.pyw"); //��� ���� �� �������� ���������
            //system(cmd);

        }
    });
}

bool recover::check_login() {//�������� ����� �� ���������� �� ��
    int data_found = 0;
    QSqlDatabase autorisation = QSqlDatabase::addDatabase("QSQLITE");
    autorisation.setDatabaseName("dbcompany.sqlite");
    autorisation.open();
    QSqlQuery query = QSqlQuery(autorisation);
    if (query.exec(u8"SELECT * FROM ��������")) {
        while (query.next()) {
            if (query.value(2) == ui->email->text().toLower()) {
                QFile file("settings2.py");
                if (file.open(QIODevice::WriteOnly)) /*��������� ���� � ������ ������ ��� ������. � ���� �*/
                {
                    QByteArray data;
                    data = QString("\nto_email = ").toUtf8();
                    data.append(QString("'%1'").arg(ui->email->text().toLower()));
                    data.append(QString("\n"));
                    data.append(QString("subject = "));
                    data.append(QString(u8"'�������������� ������'"));
                    data.append(QString("\n"));
                    data.append(QString("message = "));
                    data.append(QString(u8"'�������������� ������ ������� ������.  ��� �����: %1. ��� ������: %2.'")
                        .arg(query.value(3).toString(), query.value(4).toString()));
                    file.write(data); /*���������� ������*/
                    file.close(); /*��������� ����*/
                }

                return 1;
            }
        }
        bad_message();
        return 0;
    }
    else {
        return 1;
    }

}

void recover::bad_message() {

    QMessageBox::information(this, QObject::tr(u8"�� ������ e-mail!"), u8"������ e-mail �� ��� ������ � ���� ������,\n ����������, ������� ���������� e-mail.");
}

recover::~recover()
{
}
