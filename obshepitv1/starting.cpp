#include "starting.h"
#include "registration.h"
#include "recover.h"
#include <QtWidgets/QMainWindow>
#include <QSettings>
#include "QtSql\qsql.h"
#include <QtDebug>
#include <QFileInfo>
#include "QtSql\QSqlDatabase"
#include "QtSql\QSqlQuery"
#include "QtSql\qsqlerror.h"
#include <QtSql\QSqlQueryModel>
#include <QMessageBox>
#include <QtCore\qprocess.h>
//Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin);
/*struct starting::startingPD :public Ui_startingClass {
    
    startingPD() {
        //.
    }
};*/
/*
////////////////////////////////////////////////
���� ���������� ������ � �� � ��������� �����
���� ���������� ������ � �� � ��������� �����
���� ���������� ������ � �� � ��������� �����
////////////////////////////////////////////////
*/
void starting::load_settings() {
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("Autorisation");
    ui->your_login->setText(settings.value("login").toString());//�����
    ui->your_password->setText(settings.value("password").toString());//������
    ui->checkBox_auto_input->setChecked(settings.value("checkBox_autorisa").toBool());
    if (settings.value("checkBox_autorisa").toBool()) {
        run_autorisation();
        
    }

}
/*QString starting::getCloseStyleSheet()
{
    return "QToolButton { "
        "image: url(close_win.jpg);"
        "background-color: #292929; "
        "icon-size: 12px;"
        "padding-left: 10px;"
        "padding-right: 10px;"
        "padding-top: 5px;"
        "padding-bottom: 5px;"
        "border: 1px solid #292929; "
        "}"
        "QToolButton:hover {"
        "image: url(close_win.jpg); "
        "}"
        "QToolButton:pressed { "
        "image: url(close_win.jpg);"
        "background-color: #de8e37; "
        "}";
}*/

void starting::settings_window() { //��������� ������� �� ����
   QIcon winIcon("ICON.bmp");
    this->setWindowIcon(winIcon);
    QPixmap bkgnd("background3.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
    ui->pushButton->setStyleSheet("QPushButton{background: white; border:none;}"
        "QPushButton:hover{background: #505050; color:white;}");
    ui->pushButton_3->setStyleSheet("QPushButton{background: white; border:none;}"
        "QPushButton:hover{background: #505050; color:white;}");
   // this->setWindowFlags(Qt::FramelessWindowHint); // ��������� ���������� ����
    //this->setAttribute(Qt::WA_TranslucentBackground);// ������ ��� �������� ������� ����������
    //this->setStyleSheet(starting::getCloseStyleSheet());
}

starting::starting(QWidget* parent): ui(new Ui::startingClass)// : pd(new startingPD)
    
{
    
   /* QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setDatabaseName("noterius");
    db.setUserName("nota");
    db.setPassword("notadefault");
    if (!db.open())
    {
        qDebug() << "ERRor";
    }*/
    
    ui->setupUi(this);
    settings_window();
    load_settings();
    write_in_file();
    QSqlDatabase autorisation = QSqlDatabase::addDatabase("QSQLITE");
    autorisation.setDatabaseName("dbcompany.sqlite");
    //��������� � ����� ������ � ����������� � ��
    if (!autorisation.open())
    {
        //qDebug() << autorisation.lastError().text();
        ui->label_6->setText(QString::fromUtf8(u8"����������� �� �������"));
        return;
    }
    else {
        ui->label_6->setText(QString::fromUtf8(u8"�����������"));// famaly.close();
    }
   
    
    
       // QMessageBox::information(0, QObject::tr("Error!"), query.lastError().databaseText());
    
    //connect(ui->pushButton,&QPushButton::clicked, this, &starting::Clicks_button);
    this->connect(ui->pushButton, &QPushButton::clicked, [ui=ui, thisP=this] (){
        //��������� � ��������� ��������� ������ ����������� � ����� ������ � ������
        QSettings settings("settings.ini", QSettings::IniFormat);
        settings.beginGroup("Autorisation");
        settings.setValue("login", ui->your_login->text());
        settings.setValue("password", ui->your_password->text());
        settings.setValue("checkBox_autorisa", ui->checkBox_auto_input->isChecked());
        settings.endGroup();
        thisP -> starting::run_autorisation();
                
    });
    
    this->connect(ui->pushButton_3, &QPushButton::clicked, this, &starting::run_reg);
    //this->connect(Registr_company, &registration::send_data, this, &starting::send_data);
    //this->
    //connect(Registr_company, SIGNAL(registration::send_data(QString* a, QString* b)),
       // this, SLOT(starting::send_data(QString* login, QString* password)));
}

//���� ����� ���������� ����, ���� ���� ����� ������������ ������
void starting::bad_message() {
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, u8"������ �� �������!", u8"������������ ����� ��� ������\n ������ ������������ ������?",
        QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        recover_run();
        qDebug() << "Yes was clicked";
       // QApplication::quit();
    }
    else {
        qDebug() << "Yes was *not* clicked";
    }
   // QMessageBox::information(0, QObject::tr(u8"������ �� �������!"), u8"������������ ����� ��� ������");
}

void starting::good_message() {
    QMessageBox::information(this, QObject::tr(u8"�� ������� ����������"), u8"������ �������, ������� �� ��������");
}

void starting::run_autorisation() {
    int data_found = 0;
    QSqlDatabase autorisation = QSqlDatabase::addDatabase("QSQLITE");
    autorisation.setDatabaseName("dbcompany.sqlite");
    autorisation.open();
    // QString base = ui->lineEdit->text();
    // QString base1 = ui->lineEdit_2->text();
     //��������� ������ �� ����� ����� � ����� ������
    QSqlQuery query = QSqlQuery(autorisation);
    if (query.exec(u8"SELECT * FROM ��������")) {
        while (query.next()) { //�������� ������� �������������� ������
            if (query.value(3) == ui->your_login->text()) {
                if (query.value(4) == ui->your_password->text()) {
                    data_found++;
                   // mainWindow = new MainWindowAPP(this);
                   // mainWindow->show();
                    //close();
                    /*QSettings settings("settings3.ini", QSettings::IniFormat);
                    settings.beginGroup("Autorisation"); //��������� � ��������� ������ ��� ���������� �����������
                    settings.setValue("company", query.value(1));
                    settings.setValue("licenzy", query.value(7));
                    settings.endGroup();
                    */
                    QFile file("settings2.ini");
                    if (file.open(QIODevice::WriteOnly)) /*��������� ���� � ������ ������ ��� ������. � ���� �*/
                    {
                        QByteArray data;
                        data.append(QString("[Autorisation]"));
                        data.append(QString("\n"));
                        data.append(QString("company=%1").arg(query.value(1).toString()));
                        data.append(QString("\n"));
                        data.append(QString("licenzy=%1").arg(query.value(7).toString()));
                       

                        //���������� ���� ��� �� settings2!!!!!
                        /*
                        data.append(QString(u8"'����������� ����������� � ��������� ���, �������, ��� �� ������ � ����. ��� �����: %1. ��� ������: %2'")
                            .arg(ui->your_login->text(), ui->your_password->text()));
                        */

                        file.write(data); /*���������� ������*/
                        file.close(); /*��������� ����*/
                    }
                    QProcess* myProcess = new QProcess(this);
                    auto cmd = ("MainWindowAPP.exe");
                    myProcess->start(cmd);
                    exit(0);
                    //starting::good_message(); //���� �� ������� ����������� ����� ������ ������� ����� =)
                }
                else {
                    data_found++;
                    starting::bad_message();
                }
            }
        }
        if (data_found == 0) {
            starting::bad_message();
        }
    }
    else {
        qDebug() << "NO GOOD";
    }

}
void starting::write_in_file() { // ������������� ������ � ���� (�� ���������)... �������� ��� � ���� �������
    QFile file("settings.py");
    if (file.open(QIODevice::WriteOnly)) /*��������� ���� � ������ ������ ��� ������. � ���� �*/
    {
        QByteArray data;
        data = QString("from_email = ").toUtf8();
        data.append(QString("'danillobanav20112001@gmail.com'"));
        data.append(QString("\n"));
        data.append(QString("password = "));
        data.append(QString("'lfybk123danilka'"));
        data.append(QString("\n"));
        
        //���������� ���� ��� �� settings2!!!!!
        /*
        data.append(QString(u8"'����������� ����������� � ��������� ���, �������, ��� �� ������ � ����. ��� �����: %1. ��� ������: %2'")
            .arg(ui->your_login->text(), ui->your_password->text()));
        */

        file.write(data); /*���������� ������*/
        file.close(); /*��������� ����*/
    }
}

void starting::run_reg() {
    //�������� ���� ����������� � �������� ���� �����������
    Registr_company = new registration(this);
    Registr_company->show();
    close();
}

void starting::recover_run() {
    //�������� ���� ��� �������������� ������ ������� ������
    Recover = new recover(this);
    Recover->show();
}