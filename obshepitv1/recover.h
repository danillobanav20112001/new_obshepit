#pragma once

#include <QWidget>
#include "ui_recover.h"

class recover : public QWidget
{
	Q_OBJECT

public:
	recover(QWidget *parent = Q_NULLPTR);
	~recover();
	
private:
	Ui::recover *ui;
	void settings_window();
	bool check_login();
	void bad_message();
};
