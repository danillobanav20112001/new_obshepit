#include "winsowSettings.h"
#include "winsowSettingsPlugin.h"

#include <QtCore/QtPlugin>

winsowSettingsPlugin::winsowSettingsPlugin(QObject *parent)
    : QObject(parent)
{
    initialized = false;
}

void winsowSettingsPlugin::initialize(QDesignerFormEditorInterface * /*core*/)
{
    if (initialized)
        return;

    initialized = true;
}

bool winsowSettingsPlugin::isInitialized() const
{
    return initialized;
}

QWidget *winsowSettingsPlugin::createWidget(QWidget *parent)
{
    return new winsowSettings(parent);
}

QString winsowSettingsPlugin::name() const
{
    return "winsowSettings";
}

QString winsowSettingsPlugin::group() const
{
    return "My Plugins";
}

QIcon winsowSettingsPlugin::icon() const
{
    return QIcon();
}

QString winsowSettingsPlugin::toolTip() const
{
    return QString();
}

QString winsowSettingsPlugin::whatsThis() const
{
    return QString();
}

bool winsowSettingsPlugin::isContainer() const
{
    return false;
}

QString winsowSettingsPlugin::domXml() const
{
    return "<widget class=\"winsowSettings\" name=\"winsowSettings\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>100</width>\n"
        "   <height>100</height>\n"
        "  </rect>\n"
        " </property>\n"
        "</widget>\n";
}

QString winsowSettingsPlugin::includeFile() const
{
    return "winsowSettings.h";
}
