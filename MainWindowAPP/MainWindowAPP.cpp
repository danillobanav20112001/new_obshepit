#include "MainWindowAPP.h"
#include <QtWidgets/QApplication>
#include <qdesktopwidget.h>
#include "QRect"
#include <QSettings>
#include "QtSql\qsql.h"
#include <QtDebug>
#include <QFileInfo>
#include "QtSql\QSqlDatabase"
#include "QtSql\QSqlQuery"
#include "QtSql\qsqlerror.h"
#include <QtSql\QSqlQueryModel>
#include <QMessageBox>
#include <QtCore\qprocess.h>
//#include <QSqlQuery>
#include <QtSql\qsqlquery.h>
const auto font_size = 8;
const auto size_qtextedit = 13;
const auto height_qtextedit = 25;
const auto height_horizontal_spare = 350;
const auto zise_off_buttom = 26;
void MainWindowAPP::settings_window() {
    
    auto height = QWidget::size().height();
    auto width = QWidget::size().width();
    // QWidget::showMaximized();
    QRect b = QApplication::desktop()->screenGeometry();
    float chislo_height = b.height() / height;
    float chislo_width = b.width() / width;
    auto size_auto = font_size * (chislo_height + chislo_width);
    auto size_qtextedit_auto = size_qtextedit * (chislo_height + chislo_width);
    auto height_qtextedit_auto = height_qtextedit * (chislo_height + chislo_width);
    ui->num1->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99); border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num2->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99);border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num3->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99);border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num4->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99);border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num5->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99);border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num6->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99);border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num7->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99);border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num8->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99);border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num9->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99); border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->num0->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99);border-style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->numDel->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99); border - style: solid;border-radius: 4px; border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->pointX3->setStyleSheet(QString("QPushButton{background: rgba(255, 255, 255, 99); border-style: solid;border-radius: 4px;border:none;font-size:%1pt;}"
        "QPushButton:hover{background: rgba(77, 77, 77, 170); color:white;}").arg(size_auto));
    ui->textEdit->setFixedHeight(height_qtextedit_auto);
    ui->textEdit->setStyleSheet(QString("QTextEdit{background: rgba(255, 255, 255, 150); border-style: solid;border-radius: 4px;font-size:%1pt;border:none;}").arg(size_qtextedit_auto));
    ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    this->setWindowFlags(Qt::FramelessWindowHint);

    this->connect(ui->toolButton, &QToolButton::clicked, [ui = ui, thisP = this]() {

        thisP->close();
    });
    // ui.frame->setStyleSheet("background-color: yellow");

    // ui.gridLayout->setContentsMargins(0, 0, 0, 0);
    ui->frame->setFixedHeight(height_qtextedit_auto);
    //ui.frame_2->setFixedHeight(height_qtextedit_auto / 2);
     //ui.frame->setFixedWidth(b.width()+10);
     //ui.frame->setFrameRect(b);

     //ui.frame->setFixedHeight(10);
    //ui.frame->setGeometry(-20, 100, 0, 0);
     //ui.frame->setGeometry(b);
    ui->horizontalSpacer_5->changeSize(height_horizontal_spare * (chislo_height)+30, 190);
    ui->toolButton->setIconSize(QSize(zise_off_buttom * (chislo_height + chislo_width),
        zise_off_buttom * (chislo_height + chislo_width)));
    QSettings settings("settings2.ini", QSettings::IniFormat);
    settings.beginGroup("Autorisation");
    QString text_for_label_info = QString(u8"%1, �������� ��: %2").arg(settings.value("company").toString(), settings.value("licenzy").toString());
    ui->label_3->setText(text_for_label_info);//�������� �������� +  ��������
   
}

MainWindowAPP::MainWindowAPP(QWidget* parent)
    : ui(new Ui::MainWindowAPPClass)
{
    
    ui->setupUi(this);
    settings_window();
    this->connect(ui->num1, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "1";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }
    });
    this->connect(ui->num2, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "2";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->num3, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "3";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->num4, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "4";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->num5, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "5";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->num6, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "6";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->num7, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "7";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->num8, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "8";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->num9, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "9";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->num0, &QPushButton::clicked, [ui = ui, thisP = this]() {
        QString text_in_textedit = ui->textEdit->toPlainText();
        text_in_textedit = text_in_textedit + "0";
        ui->textEdit->setText(text_in_textedit);
        ui->textEdit->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        if (text_in_textedit.size() >= 4) {
            thisP->load_chek_in_bd();
            thisP->del_text_in_textedit();
        }

    });
    this->connect(ui->pointX3, &QPushButton::clicked, [ui = ui, thisP = this]() {
        //��� ���������  ������ ����


    });
    this->connect(ui->numDel , &QPushButton::clicked, [ui = ui, thisP = this]() {
       
        thisP->del_text_in_textedit();
    });
   
}
void MainWindowAPP::del_text_in_textedit() {
    ui->textEdit->setText("");
}
void MainWindowAPP::load_chek_in_bd() {
   /* //��������� ��������QSqlDatabase autorisation = QSqlDatabase::addDatabase("QSQLITE");
    autorisation.setDatabaseName("dbcompany.sqlite");
    autorisation.open();
    // QString base = ui->lineEdit->text();
    // QString base1 = ui->lineEdit_2->text();
     //��������� ������ �� ����� ����� � ����� ������
    QSqlQuery query = QSqlQuery(autorisation);
    if (query.exec(u8"SELECT * FROM ��������")) {
        while (query.next()) { //�������� ������� �������������� ������
            if (query.value(3) == ui->your_login->text()) {
                if (query.value(4) == ui->your_password->text()) {
                    data_found++;
                    // mainWindow = new MainWindowAPP(this);
                    // mainWindow->show();
                     //close();
                    QProcess* myProcess = new QProcess();
                    auto cmd = ("MainWindowAPP.exe");
                    myProcess->start(cmd);
                    this->close();
                    //starting::good_message(); //���� �� ������� ����������� ����� ������ ������� ����� =)
                }
                else {
                    data_found++;
                    starting::bad_message();
                }
            }
        }
        if (data_found == 0) {
            starting::bad_message();
        }
    }
    else {
        qDebug() << "NO GOOD";
    }
    */
    QSettings settings("settings2.ini", QSettings::IniFormat);
    settings.beginGroup("Autorisation");
    auto name_company = settings.value("company").toString();
    QSqlDatabase autoriz_personal = QSqlDatabase::addDatabase("QSQLITE");
    autoriz_personal.setDatabaseName("dbpersonal.sqlite");
    if (autoriz_personal.open()) {
        qDebug() << "gg";
    }
    
    QSqlQuery query = QSqlQuery(autoriz_personal);
    qDebug() << name_company;
    if (query.exec(QString(u8"SELECT * FROM %1").arg(name_company))) {
        while (query.next()) {
            if (query.value(2) == ui->textEdit->toPlainText()) {
                
               // bad_message_login();
               // return 0;
                
                QMessageBox::information(this, QObject::tr(u8"�� �����"), u8"�� �����, ����� ���������� ��������");
            }else {
        QMessageBox::information(this, QObject::tr(u8"�������� � ������������"), u8"�� ��");
        }
            qDebug() << query.value(2);
        }
        
    }
    else {ui->textEdit->setHidden(false);
       // qDebug()<<query.lastError();
        QMessageBox::information(this, QObject::tr(u8"�������� � ������������"), u8"��������, � ������ ������ ����������� � �� �����������");
        del_text_in_textedit();
    }
   /* if (ui->textEdit->toPlainText().toInt() == 1111) {
       QMessageBox::information(this, QObject::tr(u8"������ �����"), u8"�������� �������");
    }
    else {
        QMessageBox::information(this, QObject::tr(u8"������ �� �����"), u8"������������ �� ������");
    }*/
}
