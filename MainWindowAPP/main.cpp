#include "MainWindowAPP.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindowAPP w;
    QPalette pal;
    pal.setBrush(w.backgroundRole(), QBrush(QPixmap("background3.jpg")));
    w.setPalette(pal);
   //w.resize(800, 600);
    w.showFullScreen();
    return a.exec();
}
