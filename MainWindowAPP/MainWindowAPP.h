#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MainWindowAPP.h"

class MainWindowAPP : public QMainWindow
{
    Q_OBJECT

public:
    MainWindowAPP(QWidget *parent = Q_NULLPTR);

private:
    Ui::MainWindowAPPClass *ui;
    void settings_window();
    void del_text_in_textedit();
    void load_chek_in_bd();
   
};
